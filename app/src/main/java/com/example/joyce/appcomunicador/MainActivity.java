package com.example.joyce.appcomunicador;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView nome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView nome = (TextView) findViewById(R.id.textView2);
        Intent pegando = getIntent();
        nome.setText(pegando.getStringExtra("txtCod"));
    }

    public void outroApp(View view){
        Intent i = new Intent("br.com.livroandroid.hellobarcodereader");
        startActivity(i);
    }
}
